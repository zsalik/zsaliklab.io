---
layout: post
title: "The Changeling by Victor LaValle"
date: 2019-02-06 12:58:10 -0500
categories: review novel fantasy horror
---

<p align="center">
    <a target="_blank"  href="https://geni.us/292YcbB"><img border="0" src="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&MarketPlace=CA&ASIN=0812995945&ServiceVersion=20070822&ID=AsinImage&WS=1&Format=_SL250_&tag=twocanpub-20" ></a><img src="//ir-ca.amazon-adsystem.com/e/ir?t=twocanpub-20&l=am2&o=15&a=0812995945" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
</p>

The Internet infiltrates every part of our lives nowadays, particularly here in the developed world. Smartphones with cameras and always-on internet access have become so ubiquitous that entire business categories have sprung up to take advantage of this new communication method.

But what happens when this creeping entity, that has its tentacles in all of our homes and lives, suddenly turns against us? As a father of three kids, one of my greatest concerns is being able to protect my children from the various harms that society could inflict on them. There are definitely things that I can control, but even within reality, there are things I have no control over. But what if the threat ends up having ties to the supernatural?

There is a lot of Lovecraftian sensibility in Victor LaValle's [_The Changeling_](https://geni.us/292YcbB), which is no surprise. He did, after all, pen the story [_The Ballad of Black Tom_](https://geni.us/XcvdJA), which is a retelling of H.P. Lovecraft's tale [_The Horror at Red Hook_](https://geni.us/g9MBF7P). There is a palpable sense of creeping horror as you continue pacing through this story, and it never quite leaves you once you are done reading the last word and have gone back to your life. At least, that's what happened to me!

It starts off innocently and happily enough. The first portion of the book details the childhood and early adulthood of the protagonist, named Apollo. The story takes a darker turn around a third of the way in, and without giving away any spoilers (given the title), we can say it has a lot to do with kidnapped children. In addition to the darker aspects, the plot contains some magical elements in the latter part of the story, and at first, I had a hard time reconciling these with the rest of the book taking place in modern society. Although I am intimately familiar with fantastical fiction, something about the magic didn't quite click.

This all changed when my perception of the book was corrected. I had been trying to think of this as an urban fantasy, which it is most definitely not. The answer came from the discussion of fairy tales that one of the characters has with Apollo, about two-thirds of the way into the book. Fairy tales are meant to be a society's response to fears and feelings that they don't understand. Historically, they were often very dark and foreboding (think Grimm's tales), and were used as a way to put into words those threats you couldn't quite put your finger on.

That was the answer; _The Changeling_ is a fairy tale about parenthood in the modern age. When you read it like that, everything falls into place. The aspects of the story that seemed cliche before, now make sense and help build the story.

If you've been following along, here we have a book that combines a dash of Lovecraft with a large helping of fairy tale sensibilities and deals with parenting in modern society. A winning combination, without a doubt.

This was the first story that I had read from Victor LaValle, and I have since read _The Ballad of Black Tom_, and am still impressed with his ability to use words to build an image in your mind. I am truly looking forward to seeing what else he can pull out of his considerable imagination!

---

If you enjoyed this review, please consider clicking on the Amazon links above to support this site. It really helps with book purchasing costs and with the continued development of the site. Thanks for your support!
