---
layout: post
title: "The Power by Naomi Alderman"
date: 2019-02-20
categories: review novel speculative
---

<p align="center">
    <a target="_blank"  href="https://geni.us/7Lt4a"><img border="0" src="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&MarketPlace=CA&ASIN=0316547611&ServiceVersion=20070822&ID=AsinImage&WS=1&Format=_SL250_&tag=twocanpub-20" ></a><img src="//ir-ca.amazon-adsystem.com/e/ir?t=twocanpub-20&l=am2&o=15&a=0316547611" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
</p>

Gender discrimination is a hot topic these days. And with good reason; there are plenty of stats (for those willing to see them) of there being a gender pay gap, of women being victimized and generally being treated as a lesser part of society. On top of that, there is simply an underlying systemic prejudice that everyone seems to have that women are incapable of (or need help with) fulfilling roles that are traditionally held by men. "Oh, she must have known someone really well to get there..." is probably the tamest thing people say in this type of situation, and it gets MUCH worse from there.

My wife and I recently welcomed our daughter into the world, after two sons. When we found out we were most likely going to have a daughter, I have to admit that, as a father, I was scared. I was scared in a way I had not been with our boys. A lot of it came from my knowledge of the aforementioned prejudice that our society has towards women, but on top of that, there was a deep-seated concern that I needed to be the kind of man that would show her what she should expect other men to be like. I wanted to teach her that she should expect equal respect and dignity, and the exact same treatment as any man around her. 

Of course, as parents, we do the best we can, and I will take this day by day!

The novel I am discussing today, [_The Power_](https://geni.us/7Lt4a) by Naomi Alderman, does not necessarily help with finding an answer to gender discrimination issues. It does, however, like the best speculative fiction, use a scenario that is just slightly off from the real world to shine a light of different perspective on issues we face every day.

There is a touch of Margaret Atwood's [_The Handmaid's Tale_](https://geni.us/AIVks) in this story, and with good reason. As Alderman mentions in an interview, 
>"I’d been to an Orthodox Jewish primary school where every morning the boys said, ‘Thank you God for not making me a woman.’ If you put that together with _The Handmaid’s Tale_ in your head, something will eventually go fizz! Boom!"[^1] 

Further, Atwood and Alderman worked together on this novel through the Rolex mentorship program; in fact, Atwood even suggested a certain location for some scenes which have a massive impact on the narrative.

The main portion of the story is framed by a narrative involving a male writer, Neil Adam Armon, and his female editor, named Naomi(!). More than anything else in the book, this framework really sets the tone for how the story is going to play out. The gender dynamic between these two characters is extremely poignant, bringing to mind many of the ways men undermine women in our own society.

At the beginning of the novel, it is shown that young women (between their teenage years and into their thirties) have suddenly gained the ability to generate and shoot electricity from their fingertips. This "power" comes from a new organ that has grown over their collarbones that they name the skein, due to the way it looks like a roll of thread. Not much explanation is given as to how this ability came to be, but many, many theories are presented.

The main narrative is broken down into chapters that each involve one of several characters: Allie, a teenage girl living with foster parents; Roxy, the daughter of a London mob boss; Margot, a politician in the US; Jocelyn, Margot's teenage daughter; Tatiana, the First Lady of a European nation; and the only primary male character, Olatunde Edo, a journalist. This narrative method is one of the keys to the effectiveness of this novel, as it gives you over-the-shoulder insight into the thoughts and impressions of characters at the forefront of the changes society is facing due to this global event.

The human element does not end there. None of the aforementioned characters are good, or bad... They are just people, trying to work with the hand they have been dealt. The results are, to put it mildly, fascinating. By no means is any of this presented as a utopia; throughout the world, there are women shown who fight back against the oppression they have suffered for centuries; others do their best to figure out how to adjust to this change to a new global power dynamic.

_The Power_ provides an absolutely fascinating look at what could happen if the entire population of women in the world suddenly gains complete and fully effective agency. That is perhaps its most intriguing and powerful trait, and is something that hit the right note with me as the new father of a daughter!`

[^1]: ["Margaret Atwood Mentors Naomi Alderman"](https://www.telegraph.co.uk/culture/books/bookreviews/9480151/Margaret-Atwood-mentors-Naomi-Alderman.html), _Telegraph_ 

---

If you enjoyed this review, please consider clicking on the Amazon links above to support this site. It really helps with book purchasing costs and with the continued development of the site. Thanks for your support!
