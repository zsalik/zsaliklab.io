---
layout: post
title: "Dune by Frank Herbert"
date: 2019-01-23 12:58:10 -0500
categories: review novel science-fiction
---

<p align="center">
    <a target="_blank"  href="https://geni.us/vAwq9"><img border="0" src="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&MarketPlace=CA&ASIN=0441172717&ServiceVersion=20070822&ID=AsinImage&WS=1&Format=_SL250_&tag=twocanpub-20" ></a><img src="//ir-ca.amazon-adsystem.com/e/ir?t=twocanpub-20&l=am2&o=15&a=0441172717" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
</p>

What can be said about [_Dune_](https://geni.us/vAwq9) that hasn't already been said? Since the novel's release in 1965, it has spawned numerous sequels (written by Frank Herbert himself), a multitude of prequel stories (co-written by Herbert's son Brian, and Kevin J. Anderson), and film and TV adaptations that, despite their questionable faithfulness to the original material, have gained a massive cult following.

Beyond the material that does exist, an attempt was made in the 1970s by the avant-garde director Alejandro Jodorowsky to make the first film based on _Dune_. After $2 million of the $9.5 million budget (a massive sum at the time) were spent just in pre-production, with a projected film-length of 14 hours (!), the project failed. David Lynch would later film his version of the novel, releasing it in 1984, to a lot of fan controversy over the changes made to the universe in the film. Denis Villeneuve will also be directing a new version, which as of this post, is in the beginning stages.

What is it about this novel that has been so hard to capture in film? And why does it have such staying power? Can a science fiction novel from the 1960s still be relevant today? For the purposes of this discussion, we will be focusing on the original novel _Dune_, as this author strongly feels that it can stand on its own.

One aspect of _Dune_ that the reader should pick up on immediately is the "inner conversation" that many of the primary characters have with themselves. This makes it seem like there is a constant narrative being provided to the reader, giving insight into the universe that they are now being made a part of. It works reasonably well, once you get used to it, but is likely one of the reasons this novel has been hard to film accurately. After all, how can you visually and aurally present thoughts on the screen, without making it seem contrived?

In this novel, there is also a focus on a more "civilized" society trying to control what they consider backward savages. The novel’s namesake planet, Dune (or Arrakis as it is locally called), is the only source of the spice _melange_, which is a drug that the entire Galactic Empire depends on. It is needed for faster than light space travel, and has miraculous healing and age-extending properties. However, Arrakis is populated by an old civilization known as the Fremen, who are clearly modeled after Bedouins from our own history.

To keep a watchful eye on spice production, the Galactic Emperor designates one of the great noble Houses to run the spice extraction and export processes. Up to the beginning of the novel, the sinister House Harkonnen has been running the show, but the Emperor, for some reason, decides to give the job to the noble House Atreides.

Right off the bat, you meet Paul, the heir to House Atreides, and the protagonist of this story. He is quite disappointed with having to leave his home and being relegated to a backward planet. Without giving away too much, future events will definitely make him see his experience differently.

As you delve further into the story, you begin to experience an intricately developed universe, with a deep-rooted belief system surrounding the spice and the sandworms that rule the deserts of Arrakis. Using this religion, Paul is able to get the aid of the Fremen for his mission.

If you have not read _Dune_ as of yet, what have you been doing?! But on a serious note, if you are a fan of science-fiction or fantasy in any capacity, you must read _Dune._ Many later novels by new authors have been inspired by Frank Herbert's work, but there is something about the story, atmosphere, and universe that Herbert created that stands the test of time.

---

If you enjoyed this review, please consider clicking on the Amazon links above to support this site. It really helps with book purchasing costs and with the continued development of the site. Thanks for your support!
