---
layout: post
title: "The Mistborn Trilogy by Brandon Sanderson"
date: 2019-01-09 12:58:10 -0500
categories: review series fantasy
---

<div class="row">
    <div class="column">
        <a target="_blank"  href="https://geni.us/uePpPmr"><img border="0" src="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&MarketPlace=CA&ASIN=B002GYI9C4&ServiceVersion=20070822&ID=AsinImage&WS=1&Format=_SL250_&tag=twocanpub-20" ></a><img src="//ir-ca.amazon-adsystem.com/e/ir?t=twocanpub-20&l=am2&o=15&a=B002GYI9C4" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
    </div>
    <div class="column">
        <a target="_blank"  href="https://geni.us/6NBMZr"><img border="0" src="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&MarketPlace=CA&ASIN=0765356139&ServiceVersion=20070822&ID=AsinImage&WS=1&Format=_SL250_&tag=twocanpub-20" ></a><img src="//ir-ca.amazon-adsystem.com/e/ir?t=twocanpub-20&l=am2&o=15&a=0765356139" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
    </div>
    <div class="column"> 
        <a target="_blank"  href="https://geni.us/aO0W"><img border="0" src="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&MarketPlace=CA&ASIN=0765356147&ServiceVersion=20070822&ID=AsinImage&WS=1&Format=_SL250_&tag=twocanpub-20" ></a><img src="//ir-ca.amazon-adsystem.com/e/ir?t=twocanpub-20&l=am2&o=15&a=0765356147" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
    </div>
</div>

When I first heard about Brandon Sanderson, it was a short while before he had been selected to complete _The Wheel of Time_ series after the untimely death of the original author, Robert Jordan. I had heard about the Mistborn series, but then decided to read his novel [_Elantris_](https://geni.us/ZakIWO) first, to get an idea of his style (review upcoming).

After (spoiler alert) thoroughly enjoying _Elantris_, I was STOKED to get into the Mistborn series, which at the time only had the first two books released. The premise seemed interesting; a dystopian medieval world where a prophesied hero was defeated and the "Dark Lord" won? A heist plan a la _Ocean's Eleven_ to steal a valuable treasure from the aforementioned Dark Lord? SIGN ME UP!

None of the above information is spoilers, by the way. This is simply the premise of the first book, and the story is developed on top of that quite heavily through the series.

One of the more interesting aspects of Sanderson's stories has always been the well-designed magic systems, something he has come to be known for. In this world, certain individuals have developed a mutation which gives them heightened abilities (increased strength, speed, senses, etc) when they ingest certain kinds of metal flakes. Different metals give different abilities. Most of the time, these individuals can only gain effects from one particular metal, and are called Mistings. Sometimes, however, you get Mistborn, who are people that can gain enhancements from a full array of metals, and can in fact combine effects by taking metal "cocktails." Without revealing too much, Mistborn do appear in these novels, and boy, do they ever steal the show! They are the source of some absolutely incredible fight scenes and acrobatics, the kind of stuff that is usually reserved for superhero movies.

The Mist being referred to in the title is this strange fog that envelops the world at night. There are many legends surrounding it, and it is even thought to be the source of the Mistborn's powers. Many details on this are revealed later on in the series.

A discussion of the Mistborn series would be incomplete without mentioning Sanderson's Cosmere concept. This is a shared universe where many of the books he has written are somehow connected with an overarching plot and universe, and this is slowly being revealed. When the Mistborn books originally came out, the Cosmere had not been revealed at all as of yet, but when you go back and re-read them, there are definitely little signs sprinkled throughout.

While we still wait for all aspects of the Cosmere to be revealed, I recommend that any fan of unique fantasy stories get to a bookstore immediately and read the Mistborn series. This universe is also still being built, as Sanderson is writing a second trilogy (The Wax and Wayne series), set 300 years after this first series, in a Western/industrial setting. Imagine superheroes that manipulate metal in a world with guns! On top of that, he has also announced plans to set a couple of trilogies in this world, which will take place even further in the future.

If there is one negative thing to be said about all this, it's that sometimes it can feel like too much. The Cosmere is an enormous and detailed place, and keeping track of how things are connected can be overwhelming. But Sanderson does a fair job of keeping his books individually readable, relegating most shared universe reveals to small side scenes that don't necessarily affect the main plot, with very few exceptions. This makes his books incredibly readable and enjoyable, and we look forward to the next release!

---

If you enjoyed this review, please consider clicking on the Amazon links above to support this site. It really helps with book purchasing costs and with the continued development of the site. Thanks for your support!
