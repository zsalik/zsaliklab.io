---
layout: post
title: "Strange Weather by Joe Hill"
date: 2019-03-06
categories: review anthology fantasy horror
---

<p align="center">
    <a href="https://geni.us/ckGS" target="_blank"><img border="0" src="//ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&ASIN=0062663119&Format=_SL250_&ID=AsinImage&MarketPlace=CA&ServiceVersion=20070822&WS=1&tag=twocanpub-20" ></a><img src="https://ir-ca.amazon-adsystem.com/e/ir?t=twocanpub-20&l=li3&o=15&a=0062663119" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
</p>

_Strange Weather_ was not my first Joe Hill work. That privilege belongs to the novel [_Horns_](https://geni.us/hHbjx), which was also developed into a [film](https://geo.itunes.apple.com/us/movie/horns/id1439370037?mt=6&at=1010lS6W) starring Daniel Radcliffe as the protagonist. The premise of that story, where a young man suddenly wakes up one morning with a pair of devil horns having sprouted from his forehead, piqued my interest very much. Though the movie was relatively entertaining, there was something about the first-person narrator's voice in the book that seemed to resonate with me more, and I made a point to seek out more of Hill's work in the future.

I have since read his short story collection _20th Century Ghosts_, and enjoyed that as well, so I was primed and ready for _Strange Weather_ when I heard about it!

One might be inclined to call this a political anthology of stories. After all, in his interview with Cemetery Dance, Hill goes through the various inspirations for the stories in this collection. And in some ways, it is... But it's a little more subtle than that.

In the same interview, Hill mentions that he doesn't think people would want to "pay \$20 to be preached at"[^1]. And so, he does his best to use what he is good at, writing darn good thrillers and suspense stories, to get you to think about these big issues that are affecting our society today.

### Snapshot

In this tale, Hill masterfully deals with the idea of _memory_. The primary story is from the perspective of a Silicon Valley entrepreneur, Michael Figlione, looking back to his youth in California. He recalls an encounter where his elderly neighbour, who was also his babysitter when he was younger, started showing the early stages of dementia and warned him to watch out for someone called the Polaroid Man and to not "...let him take a picture of you. Don't let him start taking things away."

This narrative plays out well, but the story doesn't end there. Hill deftly weaves in a further idea regarding memory, where he brings in the concept that all of our memory is now being pulled into our mobile devices and gadgets. We are using them as our external "memories," and we may learn to regret the choices we made that lead us here.

Fans of Hill's acclaimed Locke & Key comic series will notice some links to that as well... I won't spoil it, but if you enjoyed that series, you will appreciate that scene!

### Loaded

Loaded was the story that stuck in my mind the most. It actually has no fantastical elements at all, even less so than Rain. This tale deals with the current ongoing issue of gun violence in America, in particular how it relates to ease of access to firearms, and how that could potentially play out.

As a non-resident of the USA, I am fascinated (in a horrified way) with the iron grip (pardon the pun) guns and gun control have on the American psyche. Freedom is, of course, a good thing; but brings with it a need for responsibility.

In this story, Hill portrays a series of small events that eventually escalate to mass murder. The mundanity of the events that trigger all the death in this story really works to emphasize the horror of the results.

Oh, and _that ending..._

### Aloft

In _Aloft_, Hill introduces Aubrey, a nervous and neurotic young man who is going on a skydive jump with a group of friends. You quickly find out that they are doing this in memory of a close mutual friend that recently passed away. Aubrey thinks that this act of jumping will somehow bring him closer to one of the women in their group, who he has a crush on, even though he is terrified by the idea of jumping off a plane.

Aubrey is forced to jump out, of course, and that's when things get interesting. As he is falling, he approaches a cloud, thinking he will go through as expected. But of course, the unexpected happens. He stops, and the cloud cushions his fall.

To avoid spoiling the story further, let me just say that the rest of the narrative plays out in a sort of _Castaway_ style; it switches back and forth between scenes from Aubrey's past, and his current dilemma of being stuck on a solid cloud. Once again, Hill weaves the narrative in a way that presents Aubrey's being stuck on a cloud as a physical metaphor for having one's head in the clouds.

This tale is a little more whimsical than the other three, especially the one that came before, and definitely functions well as a sort of palate cleanser; but it's a very enjoyable story in its own right!

### Rain

In _Rain_, Hill deals with the current debate around climate change, in a way only he can. In a small town in Colorado, the residents suddenly fall victim to a strange crystalline rainfall. Sadly, the death toll quickly piles up, and many causes for this rain are presented by the characters in the story. Humorously, on the other hand, the President of the USA in the story is presented as being very active on social media and giving his own ideas on where the rain came from!

Hill manages to deal with a whole bunch of social issues in this story, and I invite you to explore them on your own. One of them even involves a doomsday cult!

<br>

_Strange Weather_ is an excellent collection of short, meaty stories from an author who normally writes epic novels. We hope that the tales in this anthology will make you think as much as they did us!

[^1]: ["An Interview With Joe Hill"](https://www.cemeterydance.com/extras/an-interview-with-joe-hill/), _Cemetery Dance Online_

---

If you enjoyed this review, please consider clicking on the Amazon links above to support this site. It really helps with book purchasing costs and with the continued development of the site. Thanks for your support!
