---
layout: page
title: Reading List
---

This is a list of books we are planning to read and discuss in the near future, in no particular order. We will continue to add more as we think of them, and remove titles as they are discussed.

Reach out to us on our [Twitter account](https://twitter.com/lfowblog) if you'd like to make some suggestions!

[_NOS4A2_](https://geni.us/ECcK7Yd) by Joe Hill

[_A Darker Shade of Magic_](https://geni.us/6iUx) by V.E. Schwab

[_The Priory of the Orange Tree_](https://geni.us/JMA9m) by Samantha Shannon

[_Elantris_](https://geni.us/ZakIWO) by Brandon Sanderson

[_Horns_](https://geni.us/hHbjx) by Joe Hill