---
layout: page
title: About This Blog
---

#### Books.
For someone who reads as much as I do, I ironically feel at a loss for words for describing what they mean to me.

As an awkward kid, books (specifically stories), were a respite from what could sometimes be a very confusing world. They created a safe place where I understood the rules, and in so doing could start figuring out what the rules for the real world are, or should be.

Stories are a part of us, of our shared history as a species. In particular, fiction shows us what we dream about, what we wish the world was like, and in so doing can give us a goal to work toward, together. It helps us explain in simple terms the things that frighten us and elate us, and helps us process them, and hopefully face them and make them a part of us.

This blog is a part of my own journey through stories; it contains my thoughts and impressions of books that I read, and is an opportunity for me to share what I feel they are teaching us. It is not a review blog; there are plenty of other sites and online stores that will give you starred/scored reviews of books. I will only be posting about books that I myself have read, found to be **important**, and that were interesting enough to talk about.

I hope you will get something out of reading this as well.

Thanks for joining me on this adventure!

## Other People Who Love Books
Obviously, we are not the only site that loves reading! Here's a few of our favourite places on the Internet to learn about books, old and new.

[Overdue](https://overduepodcast.com/) - A weekly podcast about the books you've been meaning to read. The hosts, Andrew and Craig, have some detailed discussions about the background of the books they read.
<br>
<br>
[Literary Hub](https://lithub.com/) - Literally the hub (groan) for news and information regarding books, and includes some great writing and articles as well.
<br>
<br>
[Fireside Magazine](https://firesidefiction.com/) - An online speculative fiction magazine that hosts some absolutely mind-blowing short stories, novellas and flash fiction.

## Help Us Out!
Running this blog takes time and work, but it is my hope that it provides value to everyone reading it.

If you use the links below to do your purchasing, they will not add any cost (and in fact may sometimes give discounts!), but will ensure that I will be able to put content out on a regular basis. Thanks as always for all your support!

<p align="center">
    <iframe src="//rcm-na.amazon-adsystem.com/e/cm?o=15&p=48&l=ur1&category=prime&banner=1G4Z5W58289M7VG026R2&f=ifr&linkID=12ee854a52dfb05ec1254a60eb8fee47&t=twocanpub-20&tracking_id=twocanpub-20" width="728" height="90" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0"></iframe>

    <iframe src="//rcm-na.amazon-adsystem.com/e/cm?o=15&p=48&l=ur1&category=books&banner=1QXVA2AVY271Y1JAJKG2&f=ifr&linkID=469bb6db704b11cb2616cdabff48bf24&t=twocanpub-20&tracking_id=twocanpub-20" width="728" height="90" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0"></iframe>

    <iframe src="//rcm-na.amazon-adsystem.com/e/cm?o=15&p=48&l=ur1&category=books&banner=1284RDHFDY34DBJARC02&f=ifr&linkID=c74d70175411cbc5d42dff7866ed1c0d&t=twocanpub-20&tracking_id=twocanpub-20" width="728" height="90" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0"></iframe>

</p>
